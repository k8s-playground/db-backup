#!/bin/bash

set -e

echo $SPACES_KEY
sed -i "s/SPACES_REGION/${SPACES_REGION}/g" .s3cfg
sed -i "s/SPACES_KEY/${SPACES_KEY}/g" .s3cfg
sed -i "s/SPACES_SECRET/${SPACES_SECRET}/g" .s3cfg

CURRENT_DATE=$(date +%Y%m%d_%H%M%S)
FILE_NAME="backup_${BACKUP_NAME}_${CURRENT_DATE}.gz"

# TODO: add --oplog (Failed: error getting oplog start: not found)

mongodump \
  --host="${MONGO_HOST}" \
  --username="${MONGO_USERNAME}" \
  --port="${MONGO_PORT}" \
  --password="${MONGO_PASSWORD}" \
  --authenticationDatabase=admin \
  --db="${MONGO_DB}" \
  --gzip \
  --archive="${FILE_NAME}"

s3cmd -c .s3cfg put "${FILE_NAME}" "s3://${SPACES_BUCKET_NAME}"

# ./upload.py "${FILE_NAME}"
# rm "${FILE_NAME}"
